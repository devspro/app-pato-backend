package com.cft.proyect.pato.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cft.proyect.pato.entity.InfoTroopersEntity;
import com.cft.proyect.pato.entity.IntegrantesTroopersEntity;
import com.cft.proyect.pato.repository.InfoTrRepository;
import com.cft.proyect.pato.repository.IntegrantesRepository;

@Controller
@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "http://localhost:3000")
public class Controllers {
	
	@Autowired
	IntegrantesRepository integrantesRepository;
	@Autowired
	InfoTrRepository infoTr;
	
	 @GetMapping("/get/integrantes")
	    public ResponseEntity<?> obtenerIntegrantesPorEquipo() {
	        List<IntegrantesTroopersEntity> integrantes = integrantesRepository.findAll();
	        if (!integrantes.isEmpty()) {
	            return new ResponseEntity<>(integrantes, HttpStatus.OK);
	        } else {
	            return new ResponseEntity<>("No se encontraron integrantes con el equipo proporcionado", HttpStatus.NOT_FOUND);
	        }
	    }
	 
	 @GetMapping("get/info/troopers")
	 public ResponseEntity<?> obtenerInfoTroopers(){
		List<InfoTroopersEntity> infoT = infoTr.findAll();
		if (!infoT.isEmpty()) {
			 return new ResponseEntity<>(infoT, HttpStatus.OK);
		}else {
			 return new ResponseEntity<>("No se encontro informacion", HttpStatus.NOT_FOUND);
		}
		
	}

}
