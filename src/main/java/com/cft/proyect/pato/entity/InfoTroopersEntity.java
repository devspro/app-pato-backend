package com.cft.proyect.pato.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Builder;

@Entity
@Table(name="info_Troopers")
@Builder
public class InfoTroopersEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name="txt_historia")
	private String textoHistoria;
	@Column(name="txt_canchas")
	private String textoCanchas;
	@Column(name="txt_replica")
	private String textoReplicasTeam;
	
	public InfoTroopersEntity() {
		super();
	}
	public String getTextoHistoria() {
		return textoHistoria;
	}
	public void setTextoHistoria(String textoHistoria) {
		this.textoHistoria = textoHistoria;
	}
	public String getTextoCanchas() {
		return textoCanchas;
	}
	public void setTextoCanchas(String textoCanchas) {
		this.textoCanchas = textoCanchas;
	}
	public String getTextoReplicasTeam() {
		return textoReplicasTeam;
	}
	public void setTextoReplicasTeam(String textoReplicasTeam) {
		this.textoReplicasTeam = textoReplicasTeam;
	}
	
}
