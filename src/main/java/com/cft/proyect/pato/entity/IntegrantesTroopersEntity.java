package com.cft.proyect.pato.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Builder;

@Entity
@Table(name = "inte_Troopers")
@Builder
public class IntegrantesTroopersEntity {

@Id
@GeneratedValue(strategy = GenerationType.AUTO)
private Long id;

@Column(name = "nombre")
private String nombre;

@Column(name = "nickName")
private String nickName;

@Column(name = "rol")
private String rol;

@Column(name = "replicaPrimaria")
private String ReplicaPrimaria;

@Column(name = "cronoReplica")
private String cronoReplica;

@Column(name = "team")
private String team; 

public String getTeam() {
	return team;
}
public void setTeam(String team) {
	this.team = team;
}
public IntegrantesTroopersEntity() {
	super();
}
public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
public String getNombre() {
	return nombre;
}
public void setNombre(String nombre) {
	this.nombre = nombre;
}
public String getNickName() {
	return nickName;
}
public void setNickName(String nickName) {
	this.nickName = nickName;
}
public String getRol() {
	return rol;
}
public void setRol(String rol) {
	this.rol = rol;
}
public String getReplicaPrimaria() {
	return ReplicaPrimaria;
}
public void setReplicaPrimaria(String replicaPrimaria) {
	ReplicaPrimaria = replicaPrimaria;
}
public String getCronoReplica() {
	return cronoReplica;
}
public void setCronoReplica(String cronoReplica) {
	this.cronoReplica = cronoReplica;
}

}
