package com.cft.proyect.pato.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cft.proyect.pato.entity.IntegrantesTroopersEntity;

@Repository
public interface IntegrantesRepository extends JpaRepository<IntegrantesTroopersEntity, Long> {

}
