package com.cft.proyect.pato;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PatoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PatoApplication.class, args);
	}

}
